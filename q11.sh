#!/bin/bash

read symbol

read n

for ((i=0; i<$n; i++))
do
	read x
	a[$i]=$x
done

case $symbol in
		
	"+") 
		result=0
		for ((i=0; i<$n; i++))
		do
		
			result=`echo "$result + ${a[$i]}" | bc`
		done
	;;

	"-")

			result=${a[0]}
		for ((i=1; i<$n; i++))
		do
			result=`echo "$result - ${a[$i]}" | bc`
		done
	;;

	"*")
		result=1
		for ((i=0; i<$n; i++))
		do
			
			result=`echo "scale=10; $result * ${a[$i]}" | bc`
		done
	
	;;

	"/")
		result=${a[0]}
		for ((i=1; i<$n; i++))
		do
		result=`echo "scale=10; $result / ${a[$i]}" | bc`
		done
	;;

esac

printf %.4f $result
			
