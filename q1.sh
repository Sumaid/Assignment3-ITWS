#!/bin/bash


unsorted=($(compgen -c))

for word in ${unsorted[@]}
do
sorted+=(`echo $word | grep -o . | sort | tr -d "\n"`)
done

read input

sinput=`echo $input | grep -o . | sort | tr -d "\n"`

for ((k=0; k<3000; k++))
do
if [ "$sinput" == "${sorted[$k]}" ]
then
        echo yes
	echo ${unsorted[$k]}
        ${unsorted[$k]} --help
fi
done

