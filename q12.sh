#!/bin/bash

echo Type date
read input
echo Type date format
read format


year=`echo $input | tr -d ' ' | cut -d ',' -f2`
month=`echo $input | cut -d ' ' -f1` 
date=`echo $input | cut -d ',' -f1 | tr -d [a-z][A-Z]`

if [ $date -lt 10 ]
then
	datee=`echo 0$date` 
else
	datee=$date
fi

invalid=0
case ${month,,} in
	january) month=01
		if [ $date -gt 31 ]
		then 
			invalid=1
		fi;;
	jan) month=01
	 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	february) month=02
		kill1=$((year % 4))
		if [ $kill1 -eq 0 ]
		then
			if [ $date -gt 29 ]
			then
				invalid=1
			fi
		else
		if [ $date -gt 28 ]
		then
			invalid=1
		fi
		fi;;

	feb) month=02
	                                kill1=$((year % 4))
                if [ $kill1 -eq 0 ]
                then
                        if [ $date -gt 29 ] 
                        then
                                invalid=1
                        fi
                else
                if [ $date -gt 28 ]
                then
                        invalid=1
                fi
		fi;;

	march) month=03
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	mar) month=03
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	april) month=04
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	apr) month=04
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	may) month=05
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	june) month=06
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	jun) month=06
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	july) month=07
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	jul) month=07
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	august) month=08
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	aug) month=08
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	september) month=09
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	sept) month=09
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	october) month=10
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	oct) month=10
                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	november) month=11
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	nov) month=11
		                 if [ $date -gt 30 ]
                then
                        invalid=1
                fi;;

	december) month=12
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

	dec) month=12
		                 if [ $date -gt 31 ]
                then
                        invalid=1
                fi;;

esac

format=`echo $format | sed "s/MM/$month/g"`
format=`echo $format | sed "s/DD/$datee/g" | tr -d ' '`
echo $format | sed "s/YYYY/$year/g"

if [ $invalid -eq 1 ]
then
	echo "Invalid"
fi


