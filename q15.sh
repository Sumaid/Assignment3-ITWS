#!/bin/bash

wget $1 --no-proxy -O 1.html
cat 2.html | sed -e 's/<[^>]*>//g' | tr -d '[:punct:]' |  
tr ' ' '\n'  | sort | uniq -c | tr -s ' ' > $3


wget $2 --no-proxy -O 2.html
cat 2.html | sed -e 's/<[^>]*>//g' | tr -d '[:punct:]' |
tr ' ' '\n'  | sort | uniq -c | tr -s ' ' >> $3

echo "$1" > q15_url.txt
echo "$2" >> q15_url.txt

