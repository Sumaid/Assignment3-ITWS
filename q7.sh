#!/bin/bash

read input
string=$input
n=${#string}
flag=-1
for ((i=0; i<$n; i++))
do
str1="${string:$i:1}"
str2="${string:$n-$i-1:1}"
shopt -s nocasematch
case "$str1" in
 $str2 ) 
	;;
   * ) flag=0
	;;
esac
 
done

if [ "$flag" -eq 0 ]
then
echo No
else
echo Yes
fi	
