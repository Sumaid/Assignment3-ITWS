#!/bin/bash

cat ~/.bash_history | grep -v "|" | cut -d ' ' -f1 | sort | uniq -c | tr -s ' ' | awk ' { t = $1; $1 = $2; $2 = t; print; } '

	pipes=`cat ~/.bash_history | grep "|" | wc -l`

	echo "pipe $pipes" 

