#!/bin/bash

main () {
if [[ "$line" == "@yearly" ]]
then
	echo Yes
	return 
fi

if [ "$line" == "@annually" ]
then
        echo Yes
	return 
fi

if [ "$line" == "@monthly" ]
then
        echo Yes
	return
fi

if [ "$line" == "@weekly" ]
then
        echo Yes
	return 
fi

if [ "$line" == "@daily" ]
then
        echo Yes
	return
fi

if [ "$line" == "@hourly" ]
then
        echo Yes
	return
fi

if [ "$line" == "@reboot" ]
then
        echo Yes
	return
fi


pattern=" |'"
if [[ "$line" =~ $pattern ]]
then

mina=`echo "$line" | cut -d ' ' -f1`

pin=`echo "$line" | cut -d ' ' -f2`

din=`echo "$line" | cut -d ' ' -f3`

monin=`echo "$line" | cut -d ' ' -f4`

dowin=`echo "$line" | cut -d ' ' -f5`



mincheck () {
flag=0
if [[ $min == "*" ]]
then
flag=1
fi

if [[ $min =~ ^-?[0-9]+$ ]]
then
	if [ $min -ge 0 -a $min -le 59 ]
	then
        	flag=1
	fi
fi

if [[ $min == *"/"* ]]
then
	count=`echo $min | grep -o "/" | wc -l`
	if [ $count -eq 1 ]
        then
        left=`echo $min | cut -d '/' -f1`
        right=`echo $min | cut -d '/' -f2`
                if [ $right -gt 0 ]
                then
                      if [[ $left == "*" ]]
	              then
			      flag=1
		      fi
	      	      if [[ $left =~ ^-?[0-9]+$ ]]
			then
        		if [ $left -ge 0 -a $left -le 59 ]
        		then
        	        flag=1
        		fi
		      fi
		      if [[ $left == *"-"* ]]
			then
                        count=`echo $left | grep -o "-" | wc -l`
       			 if [ $count -eq 1 ]
        		 then
        		 lefta=`echo $left | cut -d '-' -f1`
			     if [[ $lefta =~ ^-?[0-9]+$ ]]
        		     then
				 righta=`echo $left | cut -d '-' -f2`
                		if [ $lefta -le $righta -a $righta -le 59 ]
                		then
                        	flag=1
                		fi
			     fi
			  fi
		      fi

       
else
	if [[ $min == *"-"* ]]
then
        count=`echo $min | grep -o "-" | wc -l`
        if [ $count -eq 1 ]
        then
        left=`echo $min | cut -d '-' -f1`
		if [[ $left =~ ^-?[0-9]+$ ]]
		then
			right=`echo $min | cut -d '-' -f2`
                	if [ $left -le $right -a $right -le 59 ]
                	then
                        	flag=1
                	fi
		fi

        fi
fi

fi
fi
fi

}

if [[ $mina == *","* ]]
then
    flag=1
         count=`echo $mina | grep -o "," | wc -l`
	counta=$((count+1))
	for ((i=1; i<=$counta; i++))
	do
		min=`echo $mina | cut -d ',' -f$i`
		mincheck "$min"
		if [ $flag -eq 0 ]
		then
			break;
		fi
	done
else
	min=$mina
	mincheck "$min"
fi

if [ $flag -eq 0 ]
then
        check=0
else	
	check=1
fi


hrcheck () {
flag=0
if [[ $hr == "*" ]]
then
flag=1
fi

if [[ $hr =~ ^-?[0-9]+$ ]]
then
	if [ $hr -ge 0 -a $hr -le 23 ]
	then
        	flag=1
	fi
fi

if [[ $hr == *"/"* ]]
then
	count=`echo $hr | grep -o "/" | wc -l`
	if [ $count -eq 1 ]
        then
        left=`echo $hr | cut -d '/' -f1`
        right=`echo $hr | cut -d '/' -f2`
                if [ $right -gt 0 ]
                then
                      if [[ $left == "*" ]]
	              then
			      flag=1
		      fi
	      	      if [[ $left =~ ^-?[0-9]+$ ]]
			then
        		if [ $left -ge 0 -a $left -le 23 ]
        		then
        	        flag=1
        		fi
		      fi
		      if [[ $left == *"-"* ]]
			then
                        count=`echo $left | grep -o "-" | wc -l`
       			 if [ $count -eq 1 ]
        		 then
        		 lefta=`echo $left | cut -d '-' -f1`
			     if [[ $lefta =~ ^-?[0-9]+$ ]]
        		     then
				 righta=`echo $left | cut -d '-' -f2`
                		if [ $lefta -le $righta -a $righta -le 23 ]
                		then
                        	flag=1
                		fi
			     fi
			  fi
		      fi

        	fi
	fi

else
	if [[ $hr == *"-"* ]]
then
        count=`echo $hr | grep -o "-" | wc -l`
        if [ $count -eq 1 ]
        then
        left=`echo $hr | cut -d '-' -f1`
		if [[ $left =~ ^-?[0-9]+$ ]]
		then
			right=`echo $hr | cut -d '-' -f2`
                	if [ $left -le $right -a $right -le 23 ]
                	then
                        	flag=1
                	fi
		fi

        fi
fi

fi

}

if [[ $pin == *","* ]]
then
    flag=1
         count=`echo $pin | grep -o "," | wc -l`
	counta=$((count+1))
	for ((i=1; i<=$counta; i++))
	do
		hr=`echo $pin | cut -d ',' -f$i`
		hrcheck "$hr"
		if [ $flag -eq 0 ]
		then
			break;
		fi
	done
else
	hr=$pin
	hrcheck "$hr"
fi

if [ $flag -eq 0 ]
then
        check=0
fi


daycheck () {
flag=0
if [[ $day == "*" ]]
then
flag=1
fi

if [[ $day =~ ^-?[0-9]+$ ]]
then
	if [ $day -ge 1 -a $day -le 31 ]
	then
        	flag=1
	fi
fi

if [[ $day == *"/"* ]]
then
	count=`echo $day | grep -o "/" | wc -l`
	if [ $count -eq 1 ]
        then
        left=`echo $day | cut -d '/' -f1`
        right=`echo $day | cut -d '/' -f2`
                if [ $right -gt 0 ]
                then
                      if [[ $left == "*" ]]
	              then
			      flag=1
		      fi
	      	      if [[ $left =~ ^-?[0-9]+$ ]]
			then
        		if [ $left -ge 1 -a $right -le 31 ]
        		then
        	        flag=1
        		fi
		      fi
		      if [[ $left == *"-"* ]]
			then
                        count=`echo $left | grep -o "-" | wc -l`
       			 if [ $count -eq 1 ]
        		 then
        		 lefta=`echo $left | cut -d '-' -f1`
			     if [[ $lefta =~ ^-?[0-9]+$ ]]
        		     then
				 righta=`echo $left | cut -d '-' -f2`
                		if [ $lefta -le $righta -a $righta -le 31 ]
                		then
                        	flag=1
                		fi
			     fi
			  fi
		      fi

        	fi
	fi

else
	if [[ $day == *"-"* ]]
then
        count=`echo $day | grep -o "-" | wc -l`
        if [ $count -eq 1 ]
        then
        left=`echo $day | cut -d '-' -f1`
		if [[ $left =~ ^-?[0-9]+$ ]]
		then
			right=`echo $day | cut -d '-' -f2`
                	if [ $left -le $right -a $right -le 31 ]
                	then
                        	flag=1
                	fi
		fi

        fi
fi

fi

}

if [[ $din == *","* ]]
then
         count=`echo $din | grep -o "," | wc -l`
	counta=$((count+1))
	for ((i=1; i<=$counta; i++))
	do
		day=`echo $din | cut -d ',' -f$i`
		daycheck "$day"
		if [ $flag -eq 0 ]
		then
			break;
		fi
	done
else
	day=$din
	daycheck "$day"
fi

if [ $flag -eq 0 ]
then
        check=0
fi

moncheck () {
flag=0
if [[ $mon == "*" ]]
then
flag=1
fi

if [[ $mon =~ ^-?[0-9]+$ ]]
then
	if [ $mon -ge 1 -a $mon -le 12 ]
	then
        	flag=1
	fi
fi


if [[ $mon =~ ^[A-Za-z]+$ ]]
then
	case ${mon,,} in
		jan)
			flag=1
		;;
                feb)
                        flag=1
                ;;
                mar)
                        flag=1
                ;;
                apr)
                        flag=1
                ;;
                may)
                        flag=1
                ;;
                jun)
                        flag=1
                ;;
                jul)
                        flag=1
                ;;
                aug)
                        flag=1
                ;;
                sep)
                        flag=1
                ;;
                oct)
                        flag=1
                ;;
                nov)
                        flag=1
                ;;
                dec)
                        flag=1
                ;;
	esac
fi

if [[ $mon == *"/"* ]]
then
	count=`echo $mon | grep -o "/" | wc -l`
	if [ $count -eq 1 ]
        then
        left=`echo $mon | cut -d '/' -f1`
        right=`echo $mon | cut -d '/' -f2`
                if [ $right -gt 0 ]
                then
                      if [[ $left == "*" ]]
	              then
			      flag=1
		      fi
	      	      if [[ $left =~ ^-?[0-9]+$ ]]
			then
        		if [ $left -ge 1 -a $right -le 12 ]
        		then
        	        flag=1
        		fi
		      fi
		      if [[ $left == *"-"* ]]
			then
                        count=`echo $left | grep -o "-" | wc -l`
       			 if [ $count -eq 1 ]
        		 then
        		 lefta=`echo $left | cut -d '-' -f1`
			     if [[ $lefta =~ ^-?[0-9]+$ ]]
        		     then
				 righta=`echo $left | cut -d '-' -f2`
                		if [ $lefta -le $righta -a $righta -le 12 ]
                		then
                        	flag=1
                		fi
			     fi
			  fi
		      fi

        	fi
	fi

else
	if [[ $mon == *"-"* ]]
then
        count=`echo $mon | grep -o "-" | wc -l`
        if [ $count -eq 1 ]
        then
        left=`echo $mon | cut -d '-' -f1`
		if [[ $left =~ ^-?[0-9]+$ ]]
		then
			right=`echo $mon | cut -d '-' -f2`
                	if [ $left -le $right -a $right -le 31 ]
                	then
                        	flag=1
                	fi
		fi

        fi
fi

fi


}

if [[ $monin == *","* ]]
then
    flag=1
         count=`echo $monin | grep -o "," | wc -l`
	counta=$((count+1))
	for ((i=1; i<=$counta; i++))
	do
		mon=`echo $monin | cut -d ',' -f$i`
		moncheck "$mon"
		if [ $flag -eq 0 ]
		then
			break;
		fi
	done
else
	mon=$monin
	moncheck "$mon"
fi

if [ $flag -eq 0 ]
then
        check=0
fi

dowcheck () {
flag=0
if [[ $dow == "*" ]]
then
flag=1
fi


if [[ $dow =~ ^[A-Za-z]+$ ]]
then
        case ${dow,,} in
                mon)
                        flag=1
                ;;
                tue)
                        flag=1
                ;;
                wed)
                        flag=1
                ;;
                thu)
                        flag=1
                ;;
                fri)
                        flag=1
                ;;
                sat)
                        flag=1
                ;;
                sun)
                        flag=1
		;;
	esac
fi

if [[ $dow =~ ^-?[0-9]+$ ]]
then
	if [ $dow -ge 1 -a $dow -le 7 ]
	then
        	flag=1
	fi
fi

if [[ $dow == *"/"* ]]
then
	count=`echo $dow | grep -o "/" | wc -l`
	if [ $count -eq 1 ]
        then
        left=`echo $dow | cut -d '/' -f1`
        right=`echo $dow | cut -d '/' -f2`
                if [ $right -gt 0 ]
                then
                      if [[ $left == "*" ]]
	              then
			      flag=1
		      fi
	      	      if [[ $left =~ ^-?[0-9]+$ ]]
			then
        		if [ $left -ge 1 -a $right -le 31 ]
        		then
        	        flag=1
        		fi
		      fi
		      if [[ $left == *"-"* ]]
			then
                        count=`echo $left | grep -o "-" | wc -l`
       			 if [ $count -eq 1 ]
        		 then
        		 lefta=`echo $left | cut -d '-' -f1`
			     if [[ $lefta =~ ^-?[0-9]+$ ]]
        		     then
				 righta=`echo $left | cut -d '-' -f2`
                		if [ $lefta -le $righta -a $righta -le 7 ]
                		then
                        	flag=1
                		fi
			     fi
			  fi
		      fi

        	fi
	fi

else
	if [[ $dow == *"-"* ]]
then
        count=`echo $dow | grep -o "-" | wc -l`
        if [ $count -eq 1 ]
        then
        left=`echo $dow | cut -d '-' -f1`
		if [[ $left =~ ^-?[0-9]+$ ]]
		then
			right=`echo $dow | cut -d '-' -f2`
                	if [ $left -le $right -a $right -le 7 ]
                	then
                        	flag=1
                	fi
		fi

        fi
fi

fi

}

if [[ $dowin == *","* ]]
then
    flag=1
         count=`echo $dowin | grep -o "," | wc -l`
	counta=$((count+1))
	for ((i=1; i<=$counta; i++))
	do
		dow=`echo $dowin | cut -d ',' -f$i`
		dowcheck "$dow"
		if [ $flag -eq 0 ]
		then
			break;
		fi
	done
else
	dow=$dowin
	dowcheck "$dow"
fi

if [ $flag -eq 0 ]
then
	check=0
fi

	if [ $check -eq 1 ]
	then
		echo Yes
	else 
		echo No
	fi



fi
}

line="$(<$1)"
main "$line"

